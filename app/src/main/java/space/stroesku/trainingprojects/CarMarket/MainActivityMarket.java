package space.stroesku.trainingprojects.CarMarket;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.util.ArrayList;

import space.stroesku.trainingprojects.CarMarket.categoryCars.Car;
import space.stroesku.trainingprojects.CarMarket.categoryCars.CargoCar;
import space.stroesku.trainingprojects.CarMarket.categoryCars.PassengerCar;
import space.stroesku.trainingprojects.CarMarket.categoryCars.RacingCar;
import space.stroesku.trainingprojects.CarMarket.categoryCars.SportCar;
import space.stroesku.trainingprojects.CarMarket.viewmodels.MainActivityViewModel;
import space.stroesku.trainingprojects.R;


public class MainActivityMarket extends AppCompatActivity implements Creator {
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    CarsFactory carsFactory = new CarsFactory(this);
    EditText brandCarEditText;
    EditText maxSpeedEditText;
    EditText extraEditText;
    EditText extraEdTextForRacing;
    Button addButton;
    ProgressBar progressBar;
    private MainActivityViewModel mainActivityViewModel;
    Spinner spinner;
    ArrayAdapter spinnerAdapter;
    ArrayList<String> spinnerArrayList;
    EnumCar type;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_market);
        Log.i("trace", "create");
//        File file = new File("/data/data/space.stroesku.trainingprojects/cache/carData.txt");
//        try {
//            FileWriter fileWriter = new FileWriter(file,false);
//            fileWriter.write("");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        brandCarEditText = findViewById(R.id.brandCarEditText);
        maxSpeedEditText = findViewById(R.id.maxSpeedEditText);
        extraEditText = findViewById(R.id.extraEditText);
        extraEdTextForRacing = findViewById(R.id.extraEdTextForRacing);
        addButton = findViewById(R.id.addButton);
        progressBar = findViewById(R.id.progressBar);
        recyclerView = findViewById(R.id.recycleViewCars);

        //В new ViewModelProvider() передаем Activity(this). Тем самым мы получим доступ к провайдеру, который хранит все ViewModel для этого Activity.
        //Методом get запрашиваем у этого провайдера конкретную модель по имени класса - MyViewModel.
        mainActivityViewModel = new ViewModelProvider(this).get(MainActivityViewModel.class);
        //инициализируем модель

        mainActivityViewModel.getCars().observe(this, new Observer<ArrayList<Car>>() {
            @Override
            public void onChanged(ArrayList<Car> cars) {
                adapter.notifyDataSetChanged();
            }
        });
        mainActivityViewModel.getIsUpdating().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (aBoolean) showProgressBar();
                else {
                    hideProgressBar();
                    recyclerView.smoothScrollToPosition(mainActivityViewModel.getCars().getValue().size() - 1);
                }
            }
        });
        createSpinner();
        initRecyclerView();
    }



    public void addCarClick(View view) throws IOException {
        if (getDataFromView()!=null) {
            type = EnumCar.getEnumByType(getDataFromView().getCarCategory());
            createNewCar(carsFactory.createCar(type));
            brandCarEditText.setText("");
            maxSpeedEditText.setText("");
            extraEditText.setText("");
            extraEdTextForRacing.setText("");
        }
    }

    private Car getDataFromView () {
        Car car = null;
        if (brandCarEditText.getText().toString().isEmpty()
            || maxSpeedEditText.getText().toString().isEmpty()
            || extraEditText.getText().toString().isEmpty())
        {
            Toast.makeText(this, "Введите все данные", Toast.LENGTH_SHORT).show();
        } else {

            try {
                String category = spinner.getSelectedItem().toString();
                String brand = brandCarEditText.getText().toString();
                int maxSpeed = Integer.parseInt(maxSpeedEditText.getText().toString());
                int extra = Integer.parseInt(extraEditText.getText().toString());

                if (extraEdTextForRacing.getText().toString().isEmpty())
                    car = new Car(category, brand, maxSpeed, extra);
                else {
                    int extraRacing = Integer.parseInt(extraEdTextForRacing.getText().toString());
                    car = new Car(category, brand, maxSpeed, extra, extraRacing);
                }
            } catch (NumberFormatException e) {
                Toast.makeText(this, "Неверный формат", Toast.LENGTH_SHORT).show();
            }
        }
        return car;
    }

    public void createSpinner() {
        spinner = findViewById(R.id.spinner);
        spinnerArrayList = new ArrayList<>();
        spinnerArrayList.add("Грузовое авто");
        spinnerArrayList.add("Легковое авто");
        spinnerArrayList.add("Гоночное авто");
        spinnerArrayList.add("Спортивное авто");
        spinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, spinnerArrayList);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                extraEdTextForRacing.setVisibility(View.INVISIBLE);
                if (position == 0) {
                    extraEditText.setHint(R.string.extraStringCargo);
                }

                if (position == 1) {
                    extraEditText.setHint(R.string.extraStringPassenger);
                }
                if (position == 2) {
                    extraEditText.setHint(R.string.extraStringSport);
                    extraEdTextForRacing.setVisibility(View.VISIBLE);
                    extraEdTextForRacing.setHint(R.string.extraStringRacing);
                }
                if (position == 3) {
                    extraEditText.setHint(R.string.extraStringSport);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void createNewCar(Car car) {
        mainActivityViewModel.addNewCar(car);
        Toast.makeText(this, "Автомобиль добавлен в текстовую базу данных ", Toast.LENGTH_SHORT).show();
    }

    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    public void initRecyclerView() {
        layoutManager = new LinearLayoutManager(this);
        adapter = new RecyclerAdapter(mainActivityViewModel.getCars().getValue());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }



    @Override
    public Car createCargoCar() {
        return new CargoCar(getDataFromView().getCarCategory(),
                            getDataFromView().getCarModel(),
                            getDataFromView().getSpeedMax(),
                            getDataFromView().getExtra());
    }
    @Override
    public Car createPassengerCar() {
        return new PassengerCar(getDataFromView().getCarCategory(),
                getDataFromView().getCarModel(),
                getDataFromView().getSpeedMax(),
                getDataFromView().getExtra());
    }
    @Override
    public Car createRacingCar() {
        return new RacingCar(getDataFromView().getCarCategory(),
                getDataFromView().getCarModel(),
                getDataFromView().getSpeedMax(),
                getDataFromView().getExtra(),
                getDataFromView().getExtraRacing()
        );
    }
    @Override
    public Car createSportCar() {
        return new SportCar(getDataFromView().getCarCategory(),
                getDataFromView().getCarModel(),
                getDataFromView().getSpeedMax(),
                getDataFromView().getExtra());
    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.i("trace", "start");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("trace", "pause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("trace", "resume");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("trace", "stop");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("trace", "restart");
    }

    //destroy application
    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            mainActivityViewModel.save();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.i("trace", "destroy");
    }
}


