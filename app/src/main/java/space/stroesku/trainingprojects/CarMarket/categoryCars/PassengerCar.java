package space.stroesku.trainingprojects.CarMarket.categoryCars;

public class PassengerCar extends Car {
    public int capacityMax;
    public PassengerCar(String carCategory, String carModel, int speedMax, int capacityMax) {
        super(carCategory, carModel,  speedMax, capacityMax);
        this.capacityMax = capacityMax;
    }


    public int getCapacityMax() {
        return capacityMax;
    }

    @Override
    public String toString() {
        return  carCategory +"," +
                carModel +"," +
                speedMax +"," +
                capacityMax;
    }
}
