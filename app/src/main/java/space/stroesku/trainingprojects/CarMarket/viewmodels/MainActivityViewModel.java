package space.stroesku.trainingprojects.CarMarket.viewmodels;

import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.io.IOException;
import java.util.ArrayList;

import space.stroesku.trainingprojects.CarMarket.categoryCars.Car;
import space.stroesku.trainingprojects.CarMarket.repository.CarRepository;

public class MainActivityViewModel extends ViewModel {
    private static final String TAG = "MainActivityViewModel";
    private MutableLiveData<ArrayList<Car>> mCar;
    private CarRepository mRepo;
    private MutableLiveData<Boolean> mIsUpdating = new MutableLiveData<>();

    {
        Log.i(TAG, "instance initializer: create new instance");
        if (mCar == null) {
            mCar = new MutableLiveData<>();
        }
        mRepo = CarRepository.getInstance();
        try {
            mCar = mRepo.getCars();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void clear(){
        mCar.setValue(null);
    }


    public void addNewCar(final Car car){
        mIsUpdating.setValue(true);
        new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                //получаем текущий список и добавляем его в currentCars
                ArrayList<Car> currentCars = mCar.getValue();
                //добавляем в этот список полученую из View машину
                currentCars.add(car);
                //кладём в mCar новый список
                mCar.postValue(currentCars);
                //переключаем состояние обновления
                mIsUpdating.postValue(false);
            }
        }.execute();
    }



    public LiveData<ArrayList<Car>> getCars(){
        return mCar;
    }


    public LiveData<Boolean> getIsUpdating(){
        return mIsUpdating;
    }


    public void save() throws IOException {
    mRepo.setDataInFile(mCar);
    }
}
