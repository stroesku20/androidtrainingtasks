package space.stroesku.trainingprojects.CarMarket.categoryCars;

public class SportCar extends Car {
    public int boostTo100;

    public SportCar( String carCategory, String carModel, int speedMax, int boostTo100) {
        super(carCategory, carModel,  speedMax, boostTo100);
        this.boostTo100 = boostTo100;
    }




    public int getBoostTo100() {
        return boostTo100;
    }

    @Override
    public String toString() {
        return  carCategory +"," +
                carModel +"," +
                speedMax +"," +
                boostTo100;

    }
}
