package space.stroesku.trainingprojects.CarMarket.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import space.stroesku.trainingprojects.CarMarket.CarsFactory;
import space.stroesku.trainingprojects.CarMarket.Creator;
import space.stroesku.trainingprojects.CarMarket.EnumCar;
import space.stroesku.trainingprojects.CarMarket.categoryCars.Car;
import space.stroesku.trainingprojects.CarMarket.categoryCars.CargoCar;
import space.stroesku.trainingprojects.CarMarket.categoryCars.PassengerCar;
import space.stroesku.trainingprojects.CarMarket.categoryCars.RacingCar;
import space.stroesku.trainingprojects.CarMarket.categoryCars.SportCar;

public class CarRepository implements Creator {
    private static final String TAG = "CarRepository";
    private static CarRepository instance;
    private ArrayList<Car> carsData = new ArrayList<>();
    private MutableLiveData<ArrayList<Car>> mCarsData = new MutableLiveData<>();
    private
    File file = new File("/data/data/space.stroesku.trainingprojects/cache/carData.txt");
    EnumCar type;
    String category, model;
    int speed, extra, extra2;


    //Singleton pattern
    public static CarRepository getInstance(){
        Log.i(TAG, "getInstance: instance == null ? " + (instance == null));
        if (instance == null)
            instance = new CarRepository();
        return instance;
    }



    //Pretend to get data from a file source
    public MutableLiveData<ArrayList<Car>> getCars() throws IOException {
        getDataFromFile();

        mCarsData.setValue(carsData);
        return mCarsData;
    }

    //get data from file and rec in carsData
    public void getDataFromFile() throws IOException {
        carsData.clear();
        CarsFactory factory = new CarsFactory(this);
        String text;
        BufferedReader reader = new BufferedReader(new FileReader(file));

        while ((text = reader.readLine()) != null) {
            String[] tokensFromLine = text.split(",");
            category = tokensFromLine[0];
            model = tokensFromLine[1];
            speed = Integer.parseInt(tokensFromLine[2]);
            extra = Integer.parseInt(tokensFromLine[3]);
            if (tokensFromLine.length==5) {
                extra2 = Integer.parseInt(tokensFromLine[4]);
            }
            type = EnumCar.getEnumByType(tokensFromLine[0]);
            Car car = factory.createCar(type);
            carsData.add(car);
        }
    }

    @Override
    public Car createCargoCar()  {
        return new CargoCar(category,model,speed,extra);
    }

    @Override
    public Car createPassengerCar(){
        return new PassengerCar(category,model,speed,extra);
    }

    @Override
    public Car createRacingCar() {
        return new RacingCar(category,model,speed,extra,extra2);
    }

    @Override
    public Car createSportCar() {
        return new SportCar(category,model,speed,extra);
    }


    public void setDataInFile(MutableLiveData<ArrayList<Car>> mCars) throws IOException {
        FileWriter fileWriter = new FileWriter(file, false);
        ArrayList<Car> cars = mCars.getValue();
        for (int i = 0; i <cars.size() ; i++) {
            fileWriter.write(cars.get(i).toString()+"\n");
        }
        fileWriter.write("");
        fileWriter.close();
    }
}
