package space.stroesku.trainingprojects.CarMarket.categoryCars;

public class RacingCar extends SportCar {
    private int power;
    private int  boostTo100;

    public RacingCar(String carCategory, String carModel,  int speedMax, int boostTo100, int power) {
        super(carCategory, carModel, speedMax, boostTo100);
        this.power = power;
        this.boostTo100 = boostTo100;
    }


    public int getBoostTo100() {
        return boostTo100;
    }
    public int getExtraRacing() {
        return power;
    }



    @Override
    public String toString() {
        return  carCategory +"," +
                carModel +"," +
                speedMax +"," +
                boostTo100 +"," +
                power;
    }
}
