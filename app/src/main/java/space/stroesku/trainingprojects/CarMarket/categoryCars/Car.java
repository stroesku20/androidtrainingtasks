package space.stroesku.trainingprojects.CarMarket.categoryCars;

public class Car {
    String carCategory;
    String carModel;
    int speedMax;
    int extra;
    int extraRacing;


    public Car(String carCategory,String carModel, int speedMax, int extra) {
        this.carModel = carModel;
        this.carCategory = carCategory;
        this.speedMax = speedMax;
        this.extra = extra;
    }
    public Car(String carCategory, String carModel,  int speedMax, int extra, int extraRacing) {
        this.carModel = carModel;
        this.carCategory = carCategory;
        this.speedMax = speedMax;
        this.extra = extra;
        this.extraRacing = extraRacing;
    }

    public int getExtraRacing() {
        return extraRacing;
    }

    public String getCarCategory() {
        return carCategory;
    }
    public String getCarModel() {
        return carModel;
    }
    public int getSpeedMax() {
        return  speedMax;
    }
    public int getExtra() {
        return extra;
    }


//    @Override
//    public String toString() {
//        return "Car{" +
//                ", Тип авто='" + carCategory + '\'' +"\n"+
//                ", Модель авто='" + carModel + '\'' +"\n"+
//                ", Максимальная скорость=" + speedMax +"\n"+
//                '}';
//    }
}
