package space.stroesku.trainingprojects.CarMarket;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import space.stroesku.trainingprojects.CarMarket.categoryCars.Car;
import space.stroesku.trainingprojects.R;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder> {

    private ArrayList<Car> cars;
    RecyclerAdapter(ArrayList<Car> cars) {
        this.cars = cars;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.car_item, parent, false);
        return new RecyclerViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        Car currentCar = cars.get(position);
        holder.categoryTextView.setText(currentCar.getCarCategory());
        holder.modelTextView.setText(currentCar.getCarModel());
        holder.maxSpeedTextView.setText("Максимальная скорость :"+currentCar.getSpeedMax() + " км/ч");
        if (currentCar.getCarCategory().contains("Гоночное")){
            holder.extraTextView.setText("Разгон до 100: "+currentCar.getExtra()+" cек");
            holder.extraTextView2.setText("Мощность="+currentCar.getExtraRacing());
        }
        else if (currentCar.getCarCategory().contains("Грузовое"))
            holder.extraTextView.setText("Грузоподъёмность: "+currentCar.getExtra()+"кг");
        else if (currentCar.getCarCategory().contains("Легковое"))
            holder.extraTextView.setText("Вместимость: "+currentCar.getExtra()+"чел");
        else if (currentCar.getCarCategory().contains("Спортивное"))
            holder.extraTextView.setText("Разгон до 100: "+currentCar.getExtra()+" cек");

    }


    @Override
    public int getItemCount() {
        return cars.size();
    }


    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView categoryTextView;
        TextView modelTextView;
        TextView maxSpeedTextView;
        TextView extraTextView;
        TextView extraTextView2;

        RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);
            categoryTextView = itemView.findViewById(R.id.categoryTextView);
            modelTextView = itemView.findViewById(R.id.modelTextView);
            maxSpeedTextView = itemView.findViewById(R.id.maxSpeedTextView);
            extraTextView = itemView.findViewById(R.id.extraTextView);
            extraTextView2 = itemView.findViewById(R.id.extraTextView2);
        }
    }
}
