package space.stroesku.trainingprojects.CarMarket;

import java.io.IOException;

import space.stroesku.trainingprojects.CarMarket.categoryCars.Car;

public interface Creator {
       Car createCargoCar() throws IOException;
       Car createPassengerCar() throws IOException;
       Car createRacingCar() throws IOException;
       Car createSportCar() throws IOException;
}
