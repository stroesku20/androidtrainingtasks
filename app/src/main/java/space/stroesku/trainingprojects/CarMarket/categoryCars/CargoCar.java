package space.stroesku.trainingprojects.CarMarket.categoryCars;

public class CargoCar extends Car {
    private int baggageMax;

    public CargoCar(String carCategory, String carModel,  int speedMax, int baggageMax) {
        super(carCategory, carModel,  speedMax, baggageMax);
        this.baggageMax = baggageMax;
    }

    @Override
    public String toString() {
        return  carCategory +"," +
                carModel +"," +
                speedMax +"," +
                baggageMax;
    }

    public int getBaggageMax() {
        return baggageMax;
    }
}
