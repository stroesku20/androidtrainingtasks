package space.stroesku.trainingprojects.recycleViewPizzaRecipes.recycleViewTest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import space.stroesku.trainingprojects.R;

public class NumbersAdapter extends RecyclerView.Adapter<NumberViewHolder> {

    private  int[] arrItems; //элементы массива
    private int viewHolderCount; //счетик созданых холдеров

    public NumbersAdapter() { //конструктор принимает массив
        viewHolderCount = 0;                //инициализируем счетчик холдера
    }

    public void updateItems(int[] arrItems) {
        this.arrItems = arrItems;
        notifyDataSetChanged();
    }

    @NonNull
    @Override//создаем новые ViewHolder
    public NumberViewHolder onCreateViewHolder(@NonNull ViewGroup parent,/*parent это и есть recyclerView*/
                                                        int viewType) {
        Context context = parent.getContext();
        //слой списка
        int layoutIdForListItem = R.layout.item_list_test;

        LayoutInflater inflater = LayoutInflater.from(context); //context является наш recyclerView

        //создадим новый эл-т списка
        //чтобы создать новый java объект из xml файла нам нужен inflater
        View view = inflater.inflate(layoutIdForListItem, parent, false);
        NumberViewHolder viewHolder = new NumberViewHolder(view);
        viewHolder.getViewHolderDescription().setText("ViewHolder index: "+ viewHolderCount);
        viewHolderCount++;
        return viewHolder;
    }



    @Override //у созданых ViewHolder меняем из значения (прокручивая список мы в уже в созданый ViewHolder будем задавать новые значения)
    public void onBindViewHolder(@NonNull NumberViewHolder holder, int position) {
        //используя метод bind(связывать) будем подставлять ViewHolder новые значения
        holder.bind(arrItems[position]);
    }

    @Override//вовзращаем общее кол-во элементов в списке
    public int getItemCount() {
        return arrItems.length;
    }



}
