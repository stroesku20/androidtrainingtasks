package space.stroesku.trainingprojects.recycleViewPizzaRecipes.recycleViewTest;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import space.stroesku.trainingprojects.R;


public class RecyclerTest extends AppCompatActivity {
    EditText editText;
    NumbersAdapter adapter;
    Button button;
    int[] intArr = new int[]{};
    RecyclerView numbersList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recycler_test);
        numbersList = findViewById(R.id.numbersList); //recyclerView
        editText = findViewById(R.id.editText);
        button = findViewById(R.id.button);
        if (savedInstanceState != null){
            intArr = savedInstanceState.getIntArray("arr");
        }
        adapter = new NumbersAdapter();
        adapter.updateItems(intArr);
        numbersList.setAdapter(adapter);
        //задает то как эл-ты распологаются в recyclerView
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        //у RV должен быть layoutManager
        numbersList.setLayoutManager(layoutManager);
    }



    public void sortClick(View view) {

        String str = editText.getText().toString();
        if (str.equals("")) {
            Toast.makeText(this, "You are forget input numbers", Toast.LENGTH_SHORT).show();
            return;
        }

        String[] strArr = str.split(",");

        intArr = new int[strArr.length];
        try {
            for (int i = 0; i < strArr.length; i++) {
                intArr[i] = Integer.parseInt(strArr[i]);
            }
        } catch (Exception e){
            Toast.makeText(this, "You input illegal statement", Toast.LENGTH_SHORT).show();
            return;
        }


        sortInsertion(intArr);
        adapter.updateItems(intArr);

    }
    public int[] sortInsertion(int[] array) {
        for (int left = 0; left < array.length; left++) {
            // Вытаскиваем значение элемента
            int value = array[left];
            // Перемещаемся по элементам, которые перед вытащенным элементом
            int i = left - 1;
            for (; i >= 0; i--) {
                // Если вытащили значение меньшее — передвигаем больший элемент дальше
                if (value < array[i]) {
                    array[i + 1] = array[i];
                } else {
                    // Если вытащенный элемент больше — останавливаемся
                    break;
                }
            }
            // В освободившееся место вставляем вытащенное значение
            array[i + 1] = value;
        }
        return array;
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putIntArray("arr", intArr);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        intArr = savedInstanceState.getIntArray("arr");
    }
}


