package space.stroesku.trainingprojects.recycleViewPizzaRecipes;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import space.stroesku.trainingprojects.R;

public class PizzaItemAdapter extends RecyclerView.Adapter<PizzaItemAdapter.PizzaViewHolder> {
    private ArrayList<PizzaItem> pizzaItemArrayList;
    private Context context;

    PizzaItemAdapter(ArrayList<PizzaItem> pizzaItemArrayList, Context context) {
        this.pizzaItemArrayList = pizzaItemArrayList;
        this.context = context;
    }

    public PizzaItemAdapter(ArrayList<PizzaItem> pizzaItemArrayList) {
        this.pizzaItemArrayList = pizzaItemArrayList;
    }

    @NonNull
    @Override
    public PizzaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pizza_items, parent, false);
        return new PizzaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PizzaViewHolder holder, int position) {
    PizzaItem currentPizzaItem = pizzaItemArrayList.get(position);
    holder.imageView.setImageResource(currentPizzaItem.getImageView());
    holder.titleTextView.setText(currentPizzaItem.getTitle());
    holder.descriptionTextView.setText(currentPizzaItem.getDescription());
    holder.recipeTextView.setText("");
    }

    @Override
    public int getItemCount() {
        return pizzaItemArrayList.size();
    }

        class PizzaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imageView;
        TextView titleTextView;
        TextView descriptionTextView;
        TextView recipeTextView;

        PizzaViewHolder(@NonNull View itemView) {
            super(itemView);
        itemView.setOnClickListener(this);
        imageView = itemView.findViewById(R.id.imageView);
        titleTextView = itemView.findViewById(R.id.textViewTitle);
        descriptionTextView = itemView.findViewById(R.id.descriptionTextView);
        recipeTextView = itemView.findViewById(R.id.recipesTextView);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            PizzaItem pizzaItem = pizzaItemArrayList.get(position);
            Intent intent = new Intent(context, RecipeActivity.class);
            intent.putExtra("image", pizzaItem.getImageView());
            intent.putExtra("title", pizzaItem.getTitle());
            intent.putExtra("recipe", pizzaItem.getRecipe());
            context.startActivity(intent);
        }
    }
}
