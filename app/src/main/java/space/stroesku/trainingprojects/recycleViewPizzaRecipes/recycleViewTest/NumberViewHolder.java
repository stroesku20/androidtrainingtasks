package space.stroesku.trainingprojects.recycleViewPizzaRecipes.recycleViewTest;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import space.stroesku.trainingprojects.R;


public  class NumberViewHolder extends RecyclerView.ViewHolder{

        //обращаются к двум tw которые есть в нашем представлении
        private TextView listItemNumberView;  //шаблон числа
        private TextView viewHolderDescription;     //viewHolder id

        public NumberViewHolder(View itemView) { // конструктор в который при принимаем item
            super(itemView);
            //связываем tw шаблонов из хмл с Java объектами
            listItemNumberView = itemView.findViewById(R.id.tv_number_item);
            viewHolderDescription = itemView.findViewById(R.id.tv_holder_number);
        }

    //чтобы изменять значения у наших шаблонов создаем метод bind(связать)

        void bind(int value){
            getListItemNumberView().setText(String.valueOf(value));
        }



        TextView getListItemNumberView() {
            return listItemNumberView;
        }
        TextView getViewHolderDescription(){
            return viewHolderDescription;
        }
    }

