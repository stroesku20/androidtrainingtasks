package space.stroesku.trainingprojects.recycleViewPizzaRecipes;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import space.stroesku.trainingprojects.R;



public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    PizzaItemAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<PizzaItem> pizzaItemArrayList = new ArrayList<>();

        pizzaItemArrayList.add(new PizzaItem(R.drawable.buffalo, StringUtils.PIZZA_1_TITLE, StringUtils.PIZZA_1_DESCRIPTION, StringUtils.PIZZA_1_RECIPE));
        pizzaItemArrayList.add(new PizzaItem(R.drawable.sheet_pan_pizza, StringUtils.PIZZA_2_TITLE, StringUtils.PIZZA_2_DESCRIPTION, StringUtils.PIZZA_2_RECIPE));
        pizzaItemArrayList.add(new PizzaItem(R.drawable.salad_pizza, StringUtils.PIZZA_3_TITLE, StringUtils.PIZZA_3_DESCRIPTION, StringUtils.PIZZA_3_RECIPE));
        pizzaItemArrayList.add(new PizzaItem(R.drawable.cast_iron_pizza_with_fennel_and_sausage, StringUtils.PIZZA_4_TITLE, StringUtils.PIZZA_4_DESCRIPTION, StringUtils.PIZZA_4_RECIPE));
        pizzaItemArrayList.add(new PizzaItem(R.drawable.healthyish_superpowered_horizontal, StringUtils.PIZZA_5_TITLE, StringUtils.PIZZA_5_DESCRIPTION, StringUtils.PIZZA_5_RECIPE));
        pizzaItemArrayList.add(new PizzaItem(R.drawable.mozzarela_grandma, StringUtils.PIZZA_6_TITLE, StringUtils.PIZZA_6_DESCRIPTION, StringUtils.PIZZA_6_RECIPE));
        pizzaItemArrayList.add(new PizzaItem(R.drawable.roasted_cauliflower, StringUtils.PIZZA_7_TITLE, StringUtils.PIZZA_7_DESCRIPTION, StringUtils.PIZZA_7_RECIPE));

        recyclerView = findViewById(R.id.recycleViewPizzaRecipes);
        layoutManager = new LinearLayoutManager(this);
        adapter = new PizzaItemAdapter(pizzaItemArrayList, this);

        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);

    }
}
