package space.stroesku.trainingprojects.recycleViewPizzaRecipes;

class PizzaItem {
    private int imageView;
    private String title;
    private String description;
    private String recipe;

    PizzaItem(int imageView, String title, String description, String recipe) {
        this.imageView = imageView;
        this.title = title;
        this.description = description;
        this.recipe = recipe;
    }

    int getImageView() {
        return imageView;
    }

    String getTitle() {
        return title;
    }

    String getDescription() {
        return description;
    }

    String getRecipe() {
        return recipe;
    }
}
