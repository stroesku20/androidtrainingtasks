package space.stroesku.trainingprojects.recycleViewPizzaRecipes;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import space.stroesku.trainingprojects.R;

public class RecipeActivity extends AppCompatActivity {
    ImageView imageView;
    TextView titleRecipeTextView;
    TextView recipeTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe);
        titleRecipeTextView = findViewById(R.id.titleRecipeTextView);
        recipeTextView = findViewById(R.id.recipeTextView);
        imageView = findViewById(R.id.imageView2);


        Intent intent = getIntent();

        if (intent != null) {
           titleRecipeTextView.setText(intent.getStringExtra("title"));
           recipeTextView.setText(intent.getStringExtra("recipe"));
           Bundle bundle = getIntent().getExtras();
           if (bundle != null) {
                int image = bundle.getInt("image");
                imageView.setImageResource(image);
           }
        }
    }
}
