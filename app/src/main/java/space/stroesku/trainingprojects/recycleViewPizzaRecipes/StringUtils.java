package space.stroesku.trainingprojects.recycleViewPizzaRecipes;

 class StringUtils {
     static final String PIZZA_1_TITLE = "Buffalo Chicken Pizza";
     static final String PIZZA_1_DESCRIPTION = "Two beautiful bar foods in one pizza package. Inspiring.";
     static final String PIZZA_1_RECIPE = "A few tips: Letting the cast-iron pan preheat in a blazing hot oven will help ensure a crispy bottom crust, just be careful getting it in and out of the oven. You can use rotisserie chicken or your own leftovers. But what really makes this recipe stand out is the blue cheese dressing, which is mild enough to convert the blue cheese adverse. We used Lodge’s 17\" cast iron skillet because we like the ease of the double handles.";

     static final String PIZZA_2_TITLE = "Sheet-Pan Pizza with Brussels Sprouts and Salami";
     static final String PIZZA_2_DESCRIPTION = "There is no higher calling of the humble Brussels sprout than this cheesy, crispy, can't-stop-eating sheet-pan pizza.";
     static final String PIZZA_2_RECIPE = "There is no higher calling of the humble Brussels sprout than this sheet-pan pizza. We pre-roast them until they're well caramelized before assembling the pizza, which means all you have to do is make sure that bottom crust is good and brown before sticking the whole thing under the broiler to get the cheese bubbling. The final result is a cheesy, crispy, can’t-stop-eating balance of flavor and texture.\n" +
            "\n" +
            "All products featured on Bon Appétit are independently selected by our editors. However, when you buy something through the retail links below, we earn an affiliate commission.";

     static final String PIZZA_3_TITLE = "Salad Pizza";
     static final String PIZZA_3_DESCRIPTION =  "We swapped the red sauce for crisp lettuce tossed in a bracing dressing—and we’re into it.";
     static final String PIZZA_3_RECIPE =  "We swapped the red sauce for crisp lettuce tossed in a bracing dressing—and we’re into it. Let the dough sit out for 20-30 minutes, which will make it easier to handle and stretch. We highly recommend making a double batch of pickled onions to layer in sandwiches, salads, and tacos that need a pick-me-up.";

     static final String PIZZA_4_TITLE = "Cast-Iron Pizza with Fennel and Sausage";
     static final String PIZZA_4_DESCRIPTION = "Cooking sausage in the pan before adding the dough infuses the crust with porky flavor.";
     static final String PIZZA_4_RECIPE = "Pizza dough is usually sold as a 1-lb. ball. You only need 12 oz. for this cast-iron pizza recipe; tear off 4 oz. for making garlic knots. We used a 10\" pan, so if yours is bigger, use a bit more dough. Cooking sausage in the pan before adding the dough infuses the crust with porky flavor.";

    static final String PIZZA_5_TITLE = "Herby Pizza with Carrot Top Pesto";
    static final String PIZZA_5_DESCRIPTION = "This garlicky carrot top pesto can be used in dozens of ways (pasta sauce! salad dressing! dipping sauce!) aside from pizza.\"";
    static final String PIZZA_5_RECIPE = "This garlicky carrot top pesto can be used in dozens of ways (pasta sauce! salad dressing! dipping sauce!) aside from pizza. This recipe from Daniela Moreira of Timber Pizza Co. is part of Healthyish Superpowered, a dinner series honoring female activists and chefs across the country, in partnership with Caviar.";

    static final String PIZZA_6_TITLE = "Classic Mozzarella Grandma Pie";
    static final String PIZZA_6_DESCRIPTION = "This is the pizza marghorita of the square-shaped pie. Resist the temptation to add extra cheese; a light hand is key.";
    static final String PIZZA_6_RECIPE = "Preparation\n" +
            "Step 1\n" +
            "\n" +
            "Place a rack in lower third of oven and preheat to 525° or as high as oven will go.\n" +
            "Step 2\n" +
            "\n" +
            "Once dough has risen on baking sheet, top with mozzarella, and dot pie with tomato sauce; sprinkle with salt and red pepper flakes, if desired. Bake pie until golden brown and crisp on bottom and sides, 20–30 minutes.";
    static final String PIZZA_7_TITLE = "Roasted Cauliflower and Ricotta Grandma Pie";
    static final String PIZZA_7_DESCRIPTION = "Precooking the cauliflower and breadcrumbs means they will get toasty and crisp as the pie bakes. An extra step, yes, but well worth it.";
    static final String PIZZA_7_RECIPE = "Preparation\n" +
            "Cauliflower\n" +
            "Step 1\n" +
            "\n" +
            "Preheat oven to 400°. Toss cauliflower, lemon, anchovies, garlic, and capers with oil on a large rimmed baking sheet; season with salt and pepper.\n" +
            "Step 2\n" +
            "\n" +
            "Roast, tossing occasionally, until cauliflower is tender but not browned, about 20 minutes. Squeeze lemon juice over cauliflower and toss to coat. Discard lemon and garlic, if desired.\n" +
            "Step 3\n" +
            "\n" +
            "DO AHEAD: Precooking the cauliflower and breadcrumbs means they will get toasty and crisp as the pie bakes. An extra step, yes, but well worth it.\n" +
            "Breadcrumbs\n" +
            "Step 4\n" +
            "\n" +
            "Meanwhile, toss breadcrumbs and oil on a large rimmed baking sheet; toast, tossing occasionally, until golden brown, 6–8 minutes. Let cool; toss with Parmesan.\n" +
            "Assembly\n" +
            "Step 5\n" +
            "\n" +
            "Place a rack in lower third of oven and preheat to 525° or as high as oven will go.\n" +
            "Step 6\n" +
            "\n" +
            "Once dough has risen in baking sheet, top with mozzarella, dot with ricotta, and top with cauliflower mixture. Bake until golden brown and crisp on bottom and sides, 20–30 minutes. Top pie with toasted breadcrumbs and bake 1 minute longer.\n" +
            "Step 7\n" +
            "\n" +
            "Serve topped with parsley.";

}
